#include <dht.h>

dht DHT;

const int activationTime = 5000;
const int valvePin = 3;
const int lightSensor = A0;
const int DHT11_PIN = 2;
const int soilPin = A5;
const int soilPower = 4;

void setup() {
  pinMode(soilPin, OUTPUT);
  pinMode(valvePin, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  DHT.read11(DHT11_PIN);

  activateValve(DHT.temperature, DHT.humidity, (float)readSoil(),analogRead(lightSensor));
}

void activateValve(float airTemp, float airHum, float soilHum, float lumi) {
  bool activate = false;
  // Algoritmo básico. Se puede buscar un algoritmo más eficiente
  // en función de los históricos.
  if(airTemp > 31.0) {
    activate |= true;
  }
  if(airHum < 50.0) {
    activate |= true;
  }
  if(soilHum < 300) {
    activate |= true;
  }
  if(lumi > 75) {
    activate |= true;
  }  
  if(activate) {
    digitalWrite(valvePin, HIGH);
    delay(activationTime);
    digitalWrite(valvePin, LOW);
  }
}

int readSoil()
{
  digitalWrite(soilPower, HIGH);
  delay(10);//wait 10 milliseconds 
  val = analogRead(soilPin);
  digitalWrite(soilPower, LOW);
  return val;
}
