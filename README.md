# Huertos Inteligentes

El Huerto inteligente permite el monitoreo de las condiciones
de humedad de los cultivos para prevenir el impacto de sequía
a través de un sistema de riego automatizado que reduce el
desperdicio.

Este repositorio contiene el código fuente e 
instrucciones de armado del proyecto denominado 
"Huertos Inteligentes" desarrollado como parte de los 
Laboratorios de Resiliencia Comunitarios.

Prototipo elaborado por: Mario Gómez @ Hackerspace San Salvador

## Lista de Materiales
Para construir este proyecto necesitas los siguientes materiales:

| Nombre                          | Cantidad |
|---------------------------------|----------|
| Arduino UNO R3                  |        1 |
| Potenciometro                   |        1 |
| Sensor de humedad en suelo      |        1 |
| Fotoresistencia                 |        1 |
| Sensor de temperatura y humedad |        1 |
| Módulo de relé                  |        1 |
| Fuente de 12v                   |        1 |
| Cables jumper M/M               |       15 |
| Electroválvula 12V              |        1 |

## Diagrama de Conexión
![](./Docu/Esquematico_bb.png)

## Guía de Conexion en Breadboard
Las conexiónes que comienzan con **BB** se conectan en 
la Breadboard. Las que comienzan con **AR** se conectan 
al Arduino. Las que comienzan con **RE** indican un 
módulo de relé. Las que comienzan con **HU** indican
el módulo de humedad en el suelo. Las que comienzan con 
**VA** indican electroválvula.

| Componente                         |        |         |        |        |
|------------------------------------|--------|---------|--------|--------|
| Potenciometro                      | BB-E31 | BB-E32  | BB-E33 |        |
| Fotoresistencia                    | BB-B32 | BB-B35  |        |        |
| Jumper 1                           | BB-D32 | AR-A0   |        |        |
| Jumper 2                           | BB-V+  | AR-4    |        |        |
| Jumper 3                           | BB-V-  | HU-GND  |        |        |
| Jumper 4                           | HU-SIG | AR-A5   |        |        |
| Módulo de Humedad en Aire          | BB-F57 | BB-F58  | BB-F59 | BB-F60 |
| Jumper 5                           | BB-J57 | BB-V-   |        |        |
| Jumper 6                           | BB-J59 | AR-2    |        |        |
| Jumper 7                           | BB-J60 | BB-V+   |        |        |
| Jumper 8 (Conectar de lado a lado) | BB-V+  | BB-V+   |        |        |
| Jumper 9 (Conectar de lado a lado) | BB-V-  | BB-V-   |        |        |
| Jumper 10                          | BB-V-  | AR-GND  |        |        |
| Jumper 11                          | BB-V+  | AR-5V   |        |        |
| Jumper 12                          | AR-3   | RE-IN   |        |        |
| Jumper 13                          | AR-VIN | RE-MAIN |        |        |
| Jumper 14                          | BB-V-  | VA-1    |        |        |
| Jumper 15                          | RE-NO  | VA-2    |        |        |

## Información para Colaborar con este Proyecto
Para colaborar con este proyecto solicita acceso al canal
oficial de Slack en [https://labsresiliencia.slack.com](https://labsresiliencia.slack.com) 
y únete al canal ***#propuesta_e1***.
